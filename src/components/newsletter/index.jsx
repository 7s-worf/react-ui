import React from 'react';
import ConfirmSubscription from './_confirm_subscription';
import A from '7s/components/a';
import CenteredCard from '7s/components/centered_card';
import {withSettings} from '7s/components/settings';

class Newsletter extends React.Component {

    static get isSimple(){
        return true;
    }

    render(){
        var Component;
        switch(this.props.page){
            case 'confirm': Component = ConfirmSubscription; break;
        }
        const {settings} = this.props;
        return <CenteredCard>
            <h1 className="logo has-text-centered"><A href="/"><img src={settings.get('logo')}/></A></h1>
            <Component params={this.props.params}/>
        </CenteredCard>;
    }
}

export default withSettings(Newsletter);
