import React from 'react';
import {T} from '7s/components';
import {withActions} from '7s/components/store';
import {withRouter} from '7s/components/router';

class ConfirmSubscription extends React.Component {

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    componentDidMount(){
        const {props} = this;
        const {confirmSubscriptionActions, router} = props;
        confirmSubscriptionActions.confirm({code: router.currentParams.get('code')});
    }

    render(){
        const {props} = this;
        const {confirmSubscription, confirmSubscriptionActions, router} = props;
        const {status} = confirmSubscription;
        const disabled = status == 'confirming' || status == 'succeeded';

        const confirm = (e) => {
            e.preventDefault();
            confirmSubscriptionActions.confirm({code: router.currentParams.get('code')});
        };

        let message;
        if (status == 'confirming'){
            message = <p>
                <T k='confirming' />
            </p>;
        } else if (status == 'failed'){
            message = <p className="alert is-danger">
                {confirmSubscription.error.message}
            </p>;
        } else if (status == 'succeeded'){
            message = <p>
                <T k='success' />
            </p>;
        } else if (status == 'already-confirmed') {
            message = <p>
                <T k='already-confirmed' />
            </p>;
        } else {
            message = <p>
                <T k='text' />
            </p>;
        }
        let button;
        if (status != 'succeeded' && status != 'already-confirmed') {
            button = <button onClick={confirm} className="button is-success" disabled={disabled}>
                { disabled ? <T k='please-wait' /> : <T k='confirm' />}
            </button>;
        }
        return <div>
            {message}
            <p>
                {button}
            </p>
        </div>;
    }
}

export default withRouter(withActions(ConfirmSubscription, ['confirmSubscription']));
