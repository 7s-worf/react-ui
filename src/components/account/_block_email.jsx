import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';

class BlockEMail extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {};
    }

    render() {

        const { blockEMail, blockEMailActions, router } = this.props;
        const { status, data } = blockEMail;

        const block = (e) => {
            e.preventDefault();
            blockEMailActions.blockEMail(router.currentParams.get('code'));
        };
        let content; 
        switch (status) {
            case 'failed':
                content = <div className="notification is-danger">
                    <b>Blocking failed:</b> {data.message}
                </div>;
                break;
            case 'succeeded':
                content = <div className="notification is-success">
                    <b>Success:</b> We will not send any messages of this type
                    to this e-mail address again.
                </div>;
                break;
            case 'initialized':
                content = <span>
                    Please confirm that you do not want to receive e-mails
                    to this address again.
                    <hr />
                    <a className="button is-warning" onClick={block}>Yes, I confirm.</a>
                </span>;
                break;
            case 'blocking':
                content = <span>
                    Please wait, blocking...
                </span>;
                break;
        }

        return <div>
            {content}
        </div>;
    }
}

export default withSettings(withRouter(withActions(BlockEMail, ['user', 'blockEMail'])));
