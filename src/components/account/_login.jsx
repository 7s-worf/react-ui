import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import withForm from '7s/components/with_form';
import { Tabs, Tab } from '7s/components/tabs';
import { A, T } from '7s/components';

import GenericLoginForm from 'worf/forms/account/login/generic';

import { ErrorFor } from '7s/components/error_helpers';

class GenericLoginFields extends React.Component {
    render() {
        const { data, error, settings, setValue } = this.props;
        return <div className="field" key="trust-computer">
            <ErrorFor error={error} field='trusted' settings={settings} />
            <div className="control">
                <input id="trusted"
                    type="checkbox"
                    onChange={(e) => setValue('trusted', e.target.checked)}
                    checked={data.trusted || false}
                />
                <label className="checkbox" htmlFor="trusted">
                    <T k='account.login.stay-logged-in' />
                </label>
            </div>
        </div>;
    }
}

class Login extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.checkLogin();
        this.checkUrl();
    }

    checkLogin() {
        const { login, loginActions, user, userActions } = this.props;
        const { status: userStatus } = user;
        const { status: loginStatus, user: loggedInUser, accessToken, organization } = login;
        if (userStatus === 'logged-in') {
            setTimeout(() => {
                this.props.router.navigateToUrl('/');
            }, 700);
        } else if (loginStatus == 'logged-in') {
            userActions.setUserData(loggedInUser, accessToken, organization);
            loginActions.reset();
        }
    }

    checkUrl() {
        const { loginActions, route, router } = this.props;
        let provider = route.props.provider;
        if (provider === undefined) {
            if (loginActions.loginProvider !== undefined)
                router.replaceUrl('/login/' + loginActions.loginProvider);
        }
    }

    componentDidUpdate() {
        this.checkUrl();
        this.checkLogin();
    }

    providerName() {
        return this.props.route.props.provider ||
            this.props.route.props.defaultProvider;
    }

    render() {
        const { props } = this;
        const { user, settings, loginForm } = props;
        const providerName = this.providerName();

        const providers = settings.get('accountProviders');
        const provider = providers.get(providerName);

        const signupLink = <A key="signup-link" href="/signup">
            <T k='account.login.signup' />
        </A>;

        const tabs = [];

        if (provider === undefined) {
            return <div>
                unknown loging provider
            </div>;
        }

        if (user.status === 'logged-in')
            return <div>
                Please wait, redirecting you...
            </div>;

        for (let [name, params] of providers) {
            tabs.push(
                <Tab active={providerName == name}
                    icon={params.icon}
                    href={'/login/'+name}>
                    <T k={params.title} />
                </Tab>);
        }

        const genericFields = <GenericLoginFields
            settings={settings}
            data={loginForm.data}
            error={loginForm.error}
            valid={loginForm.valid}
            setValue={(name, value) => loginForm.set(name, value)} />;

        return <div className="login">
            <h2 className="title">Login</h2>
            <Tabs>
                {tabs}
            </Tabs>
            <div className="provider-form">
                <provider.forms.login
                    genericFields={genericFields}
                    ready={loginForm.valid}
                    genericData={loginForm.data} />
            </div>
            <hr key="sep" />
            <p key="no-account">
                <T k="account.login.no-account-text" link={signupLink} />
            </p>
        </div>;

    }
}

export default withForm(
    withSettings(withRouter(withActions(Login, ['user', 'login']))), GenericLoginForm, 'loginForm');
