import React from 'react';
import {withActions} from '7s/components/store';
import {withRouter} from '7s/components/router';

class ResetPassword extends React.Component {

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    render(){
        return <div />;
    }
}

export default withRouter(withActions(ResetPassword, ['user', 'password']));
