import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import { Tabs, Tab } from '7s/components/tabs';
import { A, T } from '7s/components';
import withForm from '7s/components/with_form';
import GenericSignupForm from 'worf/forms/account/signup/generic';

import { ErrorFor } from '7s/components/error_helpers';

class GenericSignupFields extends React.Component {
    render() {
        const { data, error, settings, setValue } = this.props;
        const termsLink = <a key="terms"
            href={settings.t('account.signup.terms.url')}
            target="_blank"
            rel="noopener noreferrer">
            <T k='account.signup.terms.title' />
        </a>;
        return <div className="field" key="accept-terms">
            <ErrorFor error={error} field='accept_terms' settings={settings} />
            <div className="control">
                <input id="accept-terms"
                    type="checkbox"
                    onChange={(e) => setValue('accept_terms', e.target.checked)}
                    checked={data.accept_terms || false}
                />
                <label className="checkbox" htmlFor="accept-terms">
                    <T k='account.signup.accept-terms' terms={termsLink} />
                </label>
            </div>
        </div>;
    }
}

class Signup extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.checkSignup();
        this.checkUrl();
    }

    checkSignup() {
        const { user, signup, signupActions, userActions } = this.props;
        const { status: userStatus } = user;
        const { user: signedUpUser, accessToken, status: signupStatus, organization } = signup;
        if (userStatus === 'logged-in') {
            setTimeout(() => {
                this.props.router.navigateToUrl('/');
            }, 700);
        } else if (signupStatus == 'logged-in') {
            userActions.setUserData(signedUpUser, accessToken, organization);
            signupActions.reset();
        }
    }

    checkUrl() {
        const { userActions, route, router } = this.props;
        let provider = route.props.provider;
        if (provider === undefined) {
            if (userActions.signupProvider !== undefined)
                router.replaceUrl('/signup/' + userActions.signupProvider);
        }
    }

    componentDidUpdate() {
        this.checkUrl();
        this.checkSignup();
    }

    providerName() {
        return this.props.route.props.provider ||
            this.props.route.props.defaultProvider;
    }


    render() {
        const { props } = this;
        const { user, settings, signup, signupForm } = props;
        const providerName = this.providerName();

        const providers = settings.get('accountProviders');
        const provider = providers.get(providerName);

        const loginLink = <A key="login-link" href="/login">
            <T k='account.login.login' />
        </A>;

        const tabs = [];

        if (provider === undefined) {
            return <div>
                unknown signup provider
            </div>;
        }

        if (user.status === 'logged-in')
            return <div>
                Please wait, redirecting you...
            </div>;

        switch (signup.status) {
            case 'logged-in':
                return <div>
                    Please wait, redirecting you...
                </div>;
            case 'pending-confirmation':
                return <div>
                    Please confirm your e-mail...
                </div>;
            case 'pending-approval':
                return <div>
                    We need to approve your request...
                </div>;
        }

        for (let [name, params] of providers) {
            tabs.push(
                <Tab href={'/signup/' + name} active={name == providerName} icon={params.icon}>
                    <T k={params.title} />
                </Tab>);
        }

        const genericFields = <GenericSignupFields
            settings={settings}
            data={signupForm.data}
            error={signupForm.error}
            valid={signupForm.valid}
            setValue={(name, value) => signupForm.set(name, value)} />;

        return <div className="signup">
            <h2 className="title">Sign Up</h2>
            <Tabs>
                {tabs}
            </Tabs>
            <div className="provider-form">
                <provider.forms.signup
                    user={user}
                    genericFields={genericFields}
                    ready={signupForm.valid}
                    genericData={signupForm.data} />
            </div>
            <hr key="sep" />
            <p key="no-account">
                <T k='account.signup.login-text' link={loginLink} />
            </p>
        </div>;

    }
}

export default withForm(
    withSettings(withRouter(withActions(Signup, ['user', 'signup']))),
    GenericSignupForm, 'signupForm'
);
