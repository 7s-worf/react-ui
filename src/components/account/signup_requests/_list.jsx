import React from 'react';
import Confirm from '7s/components/confirm';
import { withActions } from '7s/components/store';
import T from '7s/components/t';
import {List, ListHeader, ListItem, ListColumn} from '7s/components/list';
import {DropdownMenu, MenuItem} from '7s/components/dropdown';

class SignupRequestItem extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            confirmDelete: false,
        };
    }

    render() {
        const { request, deleteRequest, approveRequest, approveStatus, deleteStatus } = this.props;
        const { confirmDelete, confirmApprove } = this.state;

        const onDelete = (e) => {
            e.preventDefault();
            this.setState({ confirmDelete: true });
        };
        const onApprove = (e) => {
            e.preventDefault();
            this.setState({ confirmApprove: true });
        };

        const closeModal = () => {
            this.setState({ confirmDelete: false, confirmApprove: false });
        };

        const closeAndDelete = () => {
            this.setState({ confirmDelete: false });
            deleteRequest(request.id);
        };

        const closeAndApprove = () => {
            this.setState({ confirmApprove: false });
            approveRequest(request.id);
        };

        let confirmDeleteModal;
        let confirmApproveModal;

        if (confirmDelete)
            confirmDeleteModal = <Confirm onClose={closeModal} onCancel={closeModal} onSave={closeAndDelete}
                title={<T k='signup-request.delete.title' />}
                save={<T k='save' />}
                saveClass="is-danger"
                saveDisabled={deleteStatus === 'deleting'}
                cancel={<T k='cancel' />}>
                <T k='signup-request.delete.message' />
            </Confirm>;
        if (confirmApprove)
            confirmApproveModal = <Confirm onClose={closeModal} onCancel={closeModal} onSave={closeAndApprove}
                title={<T k='signup-request.approve.title' />}
                save={<T k='save' />}
                saveDisabled={approveStatus === 'approving'}
                cancel={<T k='cancel' />}>
                <T k='signup-request.approve.message' />
            </Confirm>;

        return <ListItem>
            {confirmDeleteModal}
            {confirmApproveModal}
            <ListColumn size="xs">
                <p>
                    <span className="date">
                        <T k='signup-request.requested'
                            args={[new Date(request.created_at).toLocaleDateString()]} />
                    </span>
                </p>
            </ListColumn>
            <ListColumn size="sm">
                <p>
                    {request.email}
                </p>
            </ListColumn>
            <ListColumn size="icon">
                <DropdownMenu>
                    <MenuItem onClick={onDelete} icon="trash">
                        <T k='signup-request.delete' />
                    </MenuItem>
                    <MenuItem onClick={onApprove} icon="check">
                        <T k='signup-request.approve' />
                    </MenuItem>
                </DropdownMenu>
            </ListColumn>
        </ListItem>;
    }
}

class SignupRequestList extends React.Component {
    render() {
        const { onChange, requests, settings, approveSignupRequest,
            deleteSignupRequest, approveSignupRequestActions,
            deleteSignupRequestActions } = this.props;

        const approveRequest = (id) => {
            const promise = approveSignupRequestActions.approveSignupRequest(id);
            promise.then(onChange);
            promise.catch(onChange);
        };

        const deleteRequest = (id) => {
            const promise = deleteSignupRequestActions.deleteSignupRequest(id);
            promise.then(onChange);
            promise.catch(onChange);
        };

        const signupRequestItems = requests.map((request) => {
            return <SignupRequestItem
                settings={settings}
                approveRequest={approveRequest}
                deleteRequest={deleteRequest}
                approveStatus={approveSignupRequest.status}
                deleteStatus={deleteSignupRequest.status}
                key={request.id}
                request={request} />;
        });
        return <List>
            <ListHeader>
                <ListColumn size="xs">
                    <T k='date' />
                </ListColumn>
                <ListColumn size="sm">
                    <T k='email' />
                </ListColumn>
                <ListColumn size="icon">
                    <T k='menu' />
                </ListColumn>
            </ListHeader>
            {signupRequestItems}
        </List>;
    }
}

export default withActions(SignupRequestList, ['deleteSignupRequest', 'approveSignupRequest']);
