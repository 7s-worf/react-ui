import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import WithLoader from '7s/components/with_loader';
import SignupRequestList from './_list';

class SignupRequests extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.loadRequests();
    }

    loadRequests(){
        const { signupRequestsActions } = this.props;
        signupRequestsActions.getSignupRequests();
    }

    render(){
        const {signupRequests} = this.props;
        const renderLoaded = () => this.renderLoaded();
        return <WithLoader 
            resources={[signupRequests]}
            renderLoaded={renderLoaded} />;

    }

    renderLoaded() {
        const {settings, signupRequests} = this.props;
        return <div>
            <SignupRequestList settings={settings}
                onChange={() => this.loadRequests()}
                requests={signupRequests.data} />
        </div>;
    }
}

export default withRouter(withActions(withSettings(SignupRequests), ['signupRequests']));
