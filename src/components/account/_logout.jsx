import React from 'react';
import {withActions} from '7s/components/store';
import {withRouter} from '7s/components/router';
import {withSettings} from '7s/components/settings';
import Message from '7s/components/message';

class Logout extends React.Component {

    constructor(props, context){
        super(props, context);
        this.checkLogout();
    }

    componentDidUpdate(){
        this.checkLogout();
    }

    checkLogout(){
        const {logoutActions, logout, user, userActions} = this.props;
        if (user.status == 'logged-in'){
            if (logout.status == 'logged-out'){
                userActions.setLoggedOut();
                logoutActions.reset();
            } else {
                logoutActions.logout();
            }
        }
    }

    render(){
        const {logout, settings, user} = this.props;
        var {status} = logout;

        if (status == 'logging-out')
            return <Message type="info">
                Please wait, logging you out...
            </Message>;

        let providerName = user.provider;

        const providers = settings.get('accountProviders');
        const provider = providers.get(providerName);

        let content;
        if (provider !== undefined && provider.forms.logout !== undefined)
            content = <provider.forms.logout />;
        
        if (content !== undefined)
            return content;
        return <Message type="success">
            You&apos;ve been successfully logged out.
        </Message>;
    }
}

export default withSettings(withRouter(withActions(Logout, ['user', 'logout', 'login'])));
