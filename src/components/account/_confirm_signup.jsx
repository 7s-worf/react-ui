import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import { T }from '7s/components';
import Message from '7s/components/message';

class ConfirmSignup extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.checkConfirm(props);
        this.state = {};
    }

    componentDidUpdate() {
        this.checkConfirm(this.props);
    }

    checkConfirm(props) {
        const { confirmSignup,
            user,
            userActions,
            confirmSignupActions } = props;

        const { status: userStatus } = user;

        const { status: signupStatus,
            user: signedUpUser,
            accessToken,
            organization } = confirmSignup;

        if (userStatus == 'logged-in' && status == 'succeeded') {
            setTimeout(() => { props.router.navigateToUrl('/'); }, 500);
        }
        else if (signupStatus == 'succeeded') {
            userActions.setUserData(signedUpUser, accessToken, organization);
            confirmSignupActions.reset();
        }
    }

    render() {
        const { props } = this;
        const { user, confirmSignup, confirmSignupActions, settings, router } = props;
        const { status } = confirmSignup;
        const disabled = status == 'confirming' || status == 'succeeded';
        const confirm = (e) => {
            e.preventDefault();
            confirmSignupActions.confirmSignup({ code: router.currentParams.get('code')});
        };

        if (user.status == 'logged-in' && status == 'initialized') {
            return <Message type="warning">
                You are logged in with another account, please log out before
                confirming the signup for this account.
            </Message>;
        }

        let message;
        if (status == 'confirming') {
            message = <p>
                <T k='account.confirm-signup.confirming' />
            </p>;
        } else if (status == 'failed') {
            message = <p className="alert is-danger">
                {confirmSignup.error.message}
            </p>;
        } else if (status == 'succeeded') {
            message = <p>
                <T k='account.confirm-signup.success' />
            </p>;
        } else {
            message = <p>
                <T k='account.confirm-signup.confirm-text' />
            </p>;
        }
        let msg;
        if (disabled)
            msg = <T k='account.please-wait' />;
        else
            msg = <T k='account.signup.confirm' />;
        return <div>
            {message}
            <p>
                <button onClick={confirm} className="button is-success" disabled={disabled}>
                    {msg}
                </button>
            </p>
        </div>;
    }
}

export default withSettings(withRouter(withActions(ConfirmSignup, ['confirmSignup', 'user'])));
