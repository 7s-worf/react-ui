import React from 'react';
import {shallow} from 'enzyme';
import Login from './_login';
import Logout from './_logout';
import {Account} from './index.jsx';

describe('Account', () => {
    const PROPS = {
        page: 'login',
        params: {},
        settings: {
            get: () => {},
        },
    };
    test('renders the login view', () => {
        const wrapper = shallow(<Account {...PROPS} />);
        expect(wrapper.exists(Login)).toBe(true);
    });
    test('renders the logout view', () => {
        const wrapper = shallow(<Account {...PROPS} page="logout" />);
        expect(wrapper.exists(Logout)).toBe(true);
    });
});
