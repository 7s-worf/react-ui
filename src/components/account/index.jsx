import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Signup from './_signup';
import Login from './_login';
import Logout from './_logout';
import ConfirmSignup from './_confirm_signup';
import ForgotPassword from './_forgot_password';
import ResetPassword from './_reset_password';
import BlockEMail from './_block_email';
import A from '7s/components/a';
import CenteredCard from '7s/components/centered_card';
import {withSettings} from '7s/components/settings';

export class Account extends Component {
    static get isSimple(){
        return true;
    }

    render(){
        var Component;
        switch(this.props.page){
            case 'login': Component = Login; break;
            case 'signup' : Component = Signup; break;
            case 'logout' : Component = Logout; break;
            case 'forgot-password' : Component = ForgotPassword; break;
            case 'reset-password' : Component = ResetPassword; break;
            case 'confirm-signup': Component = ConfirmSignup; break;
            case 'block-email': Component = BlockEMail; break;
        }
        return <CenteredCard>
            <h1 className="logo has-text-centered">
                <A href="/"><img src={this.props.settings.get('logo')}/></A>
            </h1>
            <Component params={this.props.params}/>
        </CenteredCard>;
    }
}

Account.propTypes = {
    page: PropTypes.oneOf([
        'login',
        'signup',
        'logout',
        'forgot-password',
        'reset-password',
        'confirm-signup',
        'block-email',
    ]),
    params: PropTypes.object,
    settings: PropTypes.shape({
        get: PropTypes.func.isRequired,
    }).isRequired,
};

export default withSettings(Account);
