import React from 'react';
import CenteredCard from '7s/components/centered_card';
import isSimple from '7s/components/is_simple';
import A from '7s/components/a';

const Setup = () => <CenteredCard>
    <p className="is-centered">
        <A href="/organization/create" className="is-success">I want to create a new organization</A>
    </p>
    <hr />
    <p>
        <A href="/organization/join-info" className="is-primary">
            I want to join an existing organization
        </A>
    </p>
</CenteredCard>;

export default isSimple(Setup);
