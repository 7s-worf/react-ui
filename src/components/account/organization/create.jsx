import React from 'react';
import CenteredCard from '7s/components/centered_card';
import isSimple from '7s/components/is_simple';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import OrganizationForm from './_form';

const RedirectNotice = () => (
    <CenteredCard>
        <p>
            Organization creation successful, redirecting you...
        </p>
    </CenteredCard>
);

class CreateOrganization extends React.Component {

    componentDidMount(){
        if (this.hasOrganization())
            this.redirect();
    }

    redirect(){
        // we redirect to the index page
        const { router } = this.props;
        setTimeout(() => router.navigateToUrl('/'), 700);
    }

    componentDidUpdate(prevProps){
        const {createOrganization, organizationFormActions} = this.props;
        // if the user has already an organization we redirect
        if (this.hasOrganization())
            return this.redirect();
        // if there were server-side errors we update the form errors
        if (createOrganization.status == 'failed' && 
            prevProps.createOrganization.status !== 'failed')
            organizationFormActions.update({error: createOrganization.error});
    }

    hasOrganization(){
        // checks if the user already has an organization
        const {user, createOrganization} = this.props;
        const {status} = createOrganization;
        const {organization} = user;
        if (organization !== undefined || status == 'succeeded')
            return true;
        return false;
    }

    render(){
        const {
            organizationForm,
            createOrganization,
            organizationFormActions,
            createOrganizationActions,
        } = this.props;

        const setValue = (name, value) => {
            organizationFormActions.setData(name, value);
        };

        const onSubmit = () => {
            createOrganizationActions.create(organizationForm.data);
        };

        const { creating } = createOrganization;
        const { error, data, valid } = organizationForm;

        if (this.hasOrganization())
            return <RedirectNotice />;

        return <CenteredCard>
            <OrganizationForm valid={valid}
                error={error}
                data={data}
                setValue={setValue}
                onSubmit={onSubmit}
                submitting={creating}
            />
        </CenteredCard>;
    }
}

export default withRouter(
    withActions(
        isSimple(CreateOrganization),
        ['organizationForm', 'createOrganization', 'user']));
