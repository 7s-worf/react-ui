import React from 'react';
import PropTypes from 'prop-types';
import CenteredCard from '7s/components/centered_card';
import {withActions} from '7s/components/store';
import isSimple from '7s/components/is_simple';

const JoinOrganizationInfo = ({user}) => (
    <CenteredCard>
        Please ask your organization administrator to send an invitation
        to your e-mail <strong>{user.user.email}</strong>.
    </CenteredCard>
);

JoinOrganizationInfo.propTypes = {
    user: PropTypes.shape({
        user: PropTypes.shape({
            email: PropTypes.string.isRequired,
        }),
    }),
};

export default withActions(isSimple(JoinOrganizationInfo), ['user']);
