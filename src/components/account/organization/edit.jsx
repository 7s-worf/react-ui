import React from 'react';
import CenteredCard from '7s/components/centered_card';
import {withSettings} from '7s/components/settings';
import {withActions} from '7s/components/store';
import {withRouter} from '7s/components/router';

class EditOrganization extends React.Component {

    static get isSimple(){
        return true;
    }

    constructor(props, context){
        super(props, context);
    }

    render(){
        return (
            <CenteredCard>
                Test
            </CenteredCard>
        );
    }
}

export default withSettings(withRouter(withActions(EditOrganization, ['user', 'editOrganization'])));
