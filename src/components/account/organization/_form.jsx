import React from 'react';
import { T } from '7s/components';
import { ErrorMessage, ErrorFor } from '7s/components/error_helpers';

const OrganizationForm = ({
    data,
    error,
    setValue,
    valid,
    onSubmit,
    submitting,
    organization }) => {

    let submitText;

    if (organization === undefined)
        submitText = <T k='organization.create' />;
    else
        submitText = <T k='organization.update' />;

    return <div>
        <ErrorMessage key="error-message" error={error} />
        <form onSubmit={onSubmit} id="signup" key="signup-form" >
            <fieldset disabled={submitting}>
                <div className="field" key="name">
                    <label className="label"><T k='account.name' /></label>
                    <ErrorFor error={error} field='name' />
                    <div className="control">
                        <input
                            type=""
                            id=""
                            className="input"
                            onChange={(e) => { return setValue('name', e.target.value); }}
                            value={data.name || ''}
                            name="" />
                    </div>
                </div>
                <div className="field" key="signup">
                    <div className="control">
                        <button disabled={!valid || submitting}
                            onClick={(e) => { e.preventDefault(); onSubmit(); }}
                            className="button is-primary">{submitText}</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>;
};

export default OrganizationForm;
