import React from 'react';
import {withSettings} from '7s/components/settings';
import {withActions} from '7s/components/store';
import {withRouter} from '7s/components/router';

class ForgotPassword extends React.Component {

    constructor(props, context){
        super(props, context);
        this.state = {};
    }

    render(){
        return <div />;
    }
}

export default withSettings(withRouter(withActions(ForgotPassword, ['user', 'password'])));
