import React from 'react';
import { withActions } from '7s/components/store';
import WithLoader from '7s/components/with_loader';
import {A, T} from '7s/components';
import NewAccessTokenForm from './_new';
import AccessTokenList from './_list';

class AccessTokens extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.loadTokens();
    }

    render() {
        const { accessTokens, accessTokenScopes } = this.props;
        const renderLoaded = () => this.renderLoaded();
        return <WithLoader
            resources={[accessTokens, accessTokenScopes]}
            renderLoaded={renderLoaded} />;
    }

    loadTokens() {
        const { accessTokensActions, accessTokenScopesActions } = this.props;
        accessTokenScopesActions.getScopes();
        accessTokensActions.getTokens();
    }

    renderLoaded() {
        const { accessTokens, accessTokenScopes, newToken } = this.props;

        let accessTokenForm;

        if (newToken)
            accessTokenForm = <NewAccessTokenForm
                onChange={() => this.loadTokens()}
                scopes={accessTokenScopes.data} />;

        return <div>
            {accessTokenForm}
            <AccessTokenList
                accessTokens={accessTokens.data}
                onChange={() => this.loadTokens()} />
            <br />
            <A href="/access-tokens/new"
                className="btn btn-success">
                <T k='access-token.new.title' />
            </A>
        </div>;
    }
}

export default withActions(AccessTokens,
    ['accessTokens', 'accessTokenScopes']);
