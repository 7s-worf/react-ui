import React from 'react';
import T from '7s/components/t';
import {DropdownMenu, MenuItem} from '7s/components/dropdown';
import Confirm from '7s/components/confirm';
import { withActions } from '7s/components/store';
import {List, ListHeader, ListItem, ListColumn} from '7s/components/list';

class AccessTokenItem extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            confirmDelete: false,
        };
    }

    render() {
        const { accessToken, status, deleteToken } = this.props;
        const { confirmDelete } = this.state;

        const onDelete = (e) => {
            e.preventDefault();
            this.setState({ confirmDelete: true });
        };

        const closeModal = () => {
            this.setState({ confirmDelete: false });
        };

        const confirm = () => {
            this.setState({ confirmDelete: false });
            deleteToken(accessToken.id);
        };

        let confirmDeleteModal;

        if (confirmDelete)
            confirmDeleteModal = <Confirm onClose={closeModal}
                onCancel={closeModal}
                onSave={confirm}
                title={<T k='access-token.delete.title' />}
                save={<T k='save' />}
                saveDisabled={status === 'deleting'}
                cancel={<T k='cancel' />}>
                <T k='access-token.delete.message' />
            </Confirm>;
        return <ListItem isCard={true} key={accessToken.id}>
            {confirmDeleteModal}
            <ListColumn size="xs">
                <p>
                    {accessToken.scopes.join(', ')}
                </p>
            </ListColumn>
            <ListColumn size="lg">
                <p>
                    {accessToken.description}
                </p>
            </ListColumn>
            <ListColumn size="icon">
                <DropdownMenu>
                    <MenuItem onClick={onDelete} icon="trash">
                        <T k='access-token.delete.link' />
                    </MenuItem>
                </DropdownMenu>
            </ListColumn>
        </ListItem>;
    }
}

class AccessTokenList extends React.Component {
    render() {
        const { accessTokens, deleteAccessToken,
            deleteAccessTokenActions, onChange } = this.props;
        const { status } = deleteAccessToken;
        const deleteToken = (id) => {
            const promise = deleteAccessTokenActions.deleteToken(id);
            promise.then(onChange);
            promise.catch(onChange);
        };
        const accessTokenItems = accessTokens.map((accessToken) => {
            return <AccessTokenItem status={status}
                deleteToken={deleteToken} key={accessToken.id}
                accessToken={accessToken} />;
        });
        return <List>
            <ListHeader>
                <ListColumn size="xs">
                    <T k='access-tokens.scopes' />
                </ListColumn>
                <ListColumn size="lg">
                    <T k='description' />
                </ListColumn>
                <ListColumn size="icon">
                    <T k='menu' />
                </ListColumn>
            </ListHeader>
            {accessTokenItems}
        </List>;
    }
}

export default withActions(AccessTokenList, ['deleteAccessToken']);
