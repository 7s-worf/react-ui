import React from 'react';
import Modal from '7s/components/modal';
import { ErrorMessage, ErrorFor } from '7s/components/error_helpers';
import { withRouter } from '7s/components/router';
import { withActions } from '7s/components/store';
import { withSettings } from '7s/components/settings';
import { T } from '7s/components';
import { Form, Field, Label, Input, Control, Checkbox } from '7s/components/form';

class NewAccessTokenForm extends React.Component {

    componentWillMount(){
        const {createAccessTokenActions} = this.props;
        createAccessTokenActions.reset();
    }

    render() {
        const { props } = this;
        const { router, onChange, createAccessToken,
            createAccessTokenActions, scopes, settings } = props;
        
        const {
            formData,
            error,
            valid,
            status,
            token,
        } = createAccessToken;

        const setValue = (name, value) => {
            createAccessTokenActions.setData(name, value);
        };

        const disabled = status === 'creating';

        const setScope = (name, value) => {
            let scopes = formData.scopes;
            if (scopes === undefined)
                scopes = new Set([]);
            if (value)
                scopes.add(name);
            else
                scopes.delete(name);
            setValue('scopes', scopes);
        };

        const closeModal = () => {
            router.navigateToUrl('/access-tokens');
        };
        const createToken = () => {
            const promise = createAccessTokenActions.createToken();
            promise.then(onChange);
            promise.catch(onChange);
        };
        const scopeItems = Object.keys(scopes).map((key) => {
            const scope = scopes[key];
            return <Control key={key}>
                <Checkbox name="stay-logged-in" onChange={(value) => setScope(key, value)}/>
                <Label className="checkbox" htmlFor="stay-logged-in">
                    <b>{key}</b>: {scope.description[settings.lang()]}
                </Label>
            </Control>;
        });
        if (status === 'success')
            return <Modal title={<T k='access-token.new.title' />}
                onSave={closeModal}
                save={<T k='close' />}>
                <p>
                    <T k='access-token.new.success-text' />
                </p>
                <pre>
                    <code>
                        {token.token}
                    </code>
                </pre>
            </Modal>;
        return <Modal onClose={closeModal}
            onCancel={closeModal}
            onSave={createToken}
            title={<T k='access-token.new.title'/>}
            save={<T k='save' />}
            saveDisabled={(!valid) || status === 'creating'}
            cancel={<T k='cancel' />}>
            <ErrorMessage key="error-message" error={error} settings={settings} />
            <Form disabled={disabled}>
                <Field>
                    <Label>
                        <T k='access-token.new.description' />
                    </Label>
                    <ErrorFor error={error}
                        field='description'
                        settings={settings} />
                    <Control>
                        <Input name="description"
                            onChange={(v) => setValue('description', v)}
                            value={formData.description || ''} />
                    </Control>
                </Field>
                <Field>
                    <Label>
                        <T k='access-token.new.scopes' />
                    </Label>
                    <ErrorFor error={error} field='scopes'
                        settings={settings} />
                    {scopeItems}
                </Field>
            </Form>
        </Modal>;
    }
}

export default withRouter(withActions(withSettings(NewAccessTokenForm), ['createAccessToken']));
