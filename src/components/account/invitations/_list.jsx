import React from 'react';
import Confirm from '7s/components/confirm';
import { T } from '7s/components';
import { withActions } from '7s/components/store';
import {List, ListHeader, ListItem, ListColumn} from '7s/components/list';
import {DropdownMenu, MenuItem} from '7s/components/dropdown';

class InvitationItem extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            expanded: false,
            confirmDelete: false,
        };
    }

    render() {
        const { invitation, status, deleteInvitation } = this.props;
        const { confirmDelete } = this.state;

        const onDelete = (e) => {
            e.preventDefault();
            this.setState({ confirmDelete: true });
        };

        const closeModal = (e) => {
            this.setState({ confirmDelete: false });
        };

        const confirm = () => {
            this.setState({ confirmDelete: false });
            deleteInvitation(invitation.id);
        };

        let confirmDeleteModal;

        if (confirmDelete)
            confirmDeleteModal = <Confirm onClose={closeModal} onCancel={closeModal} onSave={confirm}
                title={<T k='account.invitation.delete.title' />}
                save={<T k='account.invitation.delete.save' />}
                saveDisabled={status === 'deleting'}
                cancel={<T k='account.invitation.delete.cancel' />}>
                Do you really want to delete this invitation?
            </Confirm>;

        return <ListItem>
            {confirmDeleteModal}
            <ListColumn size="sm">
                <p>
                    {invitation.email}
                </p>
            </ListColumn>
            <ListColumn size="lg">
                <p>
                    {invitation.message}
                </p>
            </ListColumn>
            <ListColumn size="icon">
                <DropdownMenu>
                    <MenuItem onClick={onDelete} icon="trash">
                        delete this invitation
                    </MenuItem>
                </DropdownMenu>
            </ListColumn>
        </ListItem>;
    }
}

class InvitationList extends React.Component {
    render() {
        const { invitations, deleteInvitation,
            deleteInvitationActions, onChange } = this.props;
        const { status } = deleteInvitation;
        const deleteInvite = (id) => {
            const promise = deleteInvitationActions.deleteInvitation(id);
            promise.then(onChange);
            promise.catch(onChange);
        };
        const invitationItems = invitations.map((invitation) => {
            return <InvitationItem status={status}
                deleteInvitation={deleteInvite} key={invitation.id}
                invitation={invitation} />;
        });
        return <List>
            <ListHeader>
                <ListColumn size="sm">
                    E-Mail
                </ListColumn>
                <ListColumn size="lg">
                    Message
                </ListColumn>
                <ListColumn size="icon">
                    Menu
                </ListColumn>
            </ListHeader>
            {invitationItems}
        </List>;
    }
}

export default withActions(InvitationList, ['deleteInvitation']);
