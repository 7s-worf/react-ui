import React from 'react';
import { withActions } from '7s/components/store';
import WithLoader from '7s/components/with_loader';
import { A, T } from '7s/components';
import NewInvitationForm from './_new';
import InvitationList from './_list';

class Invitations extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.loadInvitations();
    }

    render() {
        const { invitations } = this.props;
        const renderLoaded = () => this.renderLoaded();
        return <WithLoader
            resources={[invitations]}
            renderLoaded={renderLoaded} />;
    }

    loadInvitations() {
        const { invitationsActions } = this.props;
        invitationsActions.getInvitations();
    }

    renderLoaded() {

        const { invitations, newInvitation } = this.props;

        let invitationForm;

        if (newInvitation)
            invitationForm = <NewInvitationForm
                onChange={() => this.loadInvitations()} />;

        return <div>
            {invitationForm}
            <InvitationList
                invitations={invitations.data}
                onChange={() => this.loadInvitations()} />
            <br />
            <A href="/invitations/new"
                className="btn btn-success">
                <T k='account.invitation.new.title' />
            </A>
        </div>;
    }
}

export default withActions(Invitations, ['invitations']);
