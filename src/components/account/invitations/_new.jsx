import React from 'react';
import Modal from '7s/components/modal';
import { T } from '7s/components';
import { ErrorMessage, ErrorFor } from '7s/components/error_helpers';
import { withRouter } from '7s/components/router';
import { withActions } from '7s/components/store';

class NewInvitationForm extends React.Component {

    componentWillMount(){
        const {createInvitationActions} = this.props;
        createInvitationActions.reset();
    }

    render() {
        const { props } = this;
        const { router, onChange, createInvitation,
            createInvitationActions } = props;
        const { formData, error, valid, status } = createInvitation;

        const setValue = (name, value) => {
            createInvitationActions.setData(name, value);
        };

        const disabled = status === 'creating';

        const closeModal = () => {
            router.navigateToUrl('/invitations');
        };

        const createInvite = () => {
            const promise = createInvitationActions.createInvitation();
            promise.then(onChange);
            promise.catch(onChange);
        };
        if (status === 'success')
            return <Modal title={<T k='account.invitation.new.title' />}
                onSave={closeModal}
                save={<T k='account.invitation.new.close' />}>
                <p>
                    <T k='account.invitation.new.success-text' />
                </p>
            </Modal>;
        return <Modal onClose={closeModal} onCancel={closeModal} onSave={createInvite}
            title={<T k='account.invitation.new.title' />}
            save={<T k='account.invitation.new.save' />}
            saveDisabled={(!valid) || status === 'creating'}
            cancel={<T k='account.invitation.new.cancel' />}>
            <ErrorMessage key="error-message" error={error} />
            <form disabled={disabled} onSubmit={(e)=>{e.preventDefault();createInvite();}} id="invitation" key="invitation-form" >
                <div className="field" key="email">
                    <label className="label"><T k='account.email' /></label>
                    <ErrorFor error={error} field='email' />
                    <div className="control">
                        <input
                            className="input"
                            onChange={(e) => {return setValue('email', e.target.value);}}
                            value={formData.email || ''}
                            name="" />
                    </div>
                </div>
                <div className="field" key="message">
                    <label className="label">
                        <T k='account.invitation.new.message' />
                    </label>
                    <ErrorFor error={error} field='message' />
                    <div className="control">
                        <input
                            className="input"
                            onChange={(e) => {return setValue('message', e.target.value);}}
                            value={formData.message || ''}
                            name="" />
                    </div>
                </div>
                <button style={{display:'none'}}>submit</button>
            </form>
        </Modal>;
    }
}

export default withRouter(withActions(NewInvitationForm,
    ['createInvitation']));
