import React from 'react';
import {T, withActions} from '7s/components';

export const Title = ({title, icon}) =>
    <span>
        <span className="icon is-small">
            <i className={'fas fa-'+icon} />
        </span>
        <span>
            <T k={['nav', title, 'title']} />
        </span>
    </span>;


const User = withActions((props) => <span>
    <span className="icon is-small"><i className="fas fa-user-circle" /></span>
    <span className="is-hidden-navbar">{props.user.user.email}</span>
</span>,['user']);

export const menu = new Map([
    ['main', new Map([
        [
            'admin',
            {
                show : (store) => {
                    return store.get('user').user !== undefined 
                           && store.get('user').user.superuser ? true : false;
                },
                title : <Title title='admin' icon='user-circle' />,
                route: 'admin',
                subMenu : [
                    {
                        route : 'invitations',
                        title : <Title title="invitations" icon="fa-envelope" />,
                    },
                    {
                        route : 'signup-requests',
                        title : <Title title="signup-requests" icon="fa-envelope-open" />,
                    },
                ],
            },
        ],
    ]),
    ],
    ['nav', new Map([
        [
            'user',
            {
                show : (store) => {
                    return store.get('user').user !== undefined;
                },
                title : <User />,
                subMenu : [
                    {
                        route : 'access-tokens',
                        title : <Title title="access-tokens" icon="key" />,
                    },
                    {
                        route : 'logout',
                        title : <Title title="logout" icon="sign-out-alt" />,
                    },
                ],
            },
        ],
    ])],
]);

export default menu;
