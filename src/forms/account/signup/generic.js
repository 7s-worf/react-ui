import Form from '7s/utils/form';


export default class GenericSignupForm extends Form {

    validate(){
        const errors = {};
        const {settings, data} = this;
        if (data.accept_terms !== true)
            errors.accept_terms = settings.t(['account', 'signup', 'terms-not-accepted']);
        return errors;
    }
}
