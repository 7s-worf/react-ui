import Form from '7s/utils/form';

const t = (settings, key, params) => {
    return settings.t(['account', 'access-token', 'new', key], params);
};

export default class AccessTokenForm extends Form {

    validate(){
        const errors = {};
        const {settings, data} = this;
        if (!data.description){
            errors.description = t(settings, 'missing-description');
        }
        if (!data.scopes || data.scopes.size == 0){
            errors.scopes = t(settings, 'choose-at-least-one-scope');
        }
        return errors;
    }
}
