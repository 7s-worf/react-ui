import Form from '7s/utils/form';

const t = (settings, key, params) => {
    return settings.t(['account', 'invitation', 'new', key], params);
};

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;

export default class InvitationForm extends Form {

    validate(){
        const errors = {};
        const {settings, data} = this;
        if (!data.message){
            errors.message = t(settings, 'missing-message');
        }
        if (!data.email){
            errors.email = t(settings, 'missing-email');
        }
        else if (!emailRegex.test(data.email)){
            errors.email = t(settings, 'invalid-email');
        }
        return errors;
    }
}
