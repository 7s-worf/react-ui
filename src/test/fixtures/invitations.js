var invitations = [
    {
        'accepted_at': '2018-06-28T09:33:10.467135Z',
        'created_at': '2018-06-28T11:28:36.087189Z',
        'email': 'max@mustermann.de',
        'id': 'a6b7d29a-0fc7-46b8-99ab-866ae4232fe0',
        'invited_user': {
            'created_at': '2018-06-28T11:33:24.855364Z',
            'disabled': false,
            'display_name': null,
            'email': 'max@mustermann.de',
            'id': '63e5b66a-5eeb-4132-be5d-74c79427dbb4',
            'language': 'en',
            'new_email': null,
            'superuser': false,
            'updated_at': '2018-06-28T11:33:24.855364Z',
        },
        'inviting_user': {
            'created_at': '2018-06-27T11:40:12.065628Z',
            'disabled': false,
            'display_name': null,
            'email': 'inviter@kiprotect.com',
            'id': '7c46b1a5-88f4-4805-8469-64cbd04a5537',
            'language': 'en',
            'new_email': null,
            'superuser': true,
            'updated_at': '2018-06-27T11:40:12.065628Z',
        },
        'message': 'Hey! Check out my cool API! :)',
        'tied_to_email': false,
        'valid': false,
        'valid_until': null,
    },
];

export default invitations;
