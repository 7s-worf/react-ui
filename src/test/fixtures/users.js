var users = [
    {
        id: 'e320a56a-25d3-472a-ade9-5682eabcfb80',
        email: 'max@mustermann.de',
        password: 'test1234',
        superuser: true,
        first_name: 'Max',
        last_name: 'Mustermann',
        organization: {
            active: true,
            description: null,
            id: 'a3742761-8d78-4c94-86d3-c9786f201c8a',
            name: 'test org',
            roles: [
                {
                    confirmed: true,
                    id: '0c1dfb8f-e3e2-470c-bed7-4d22f3cf3e17',
                    role: 'superuser',
                },
            ],
        },
        access_tokens: [
            {
                token: '12345678',
                scopes: ['admin'],
                data: {
                    provider: 'password',
                },
                description: 'Login Access Token',
                id: '648a95e2-a72b-45c4-80b5-1ad42f4d5354',
            },
        ],
    },
];

export default users;
