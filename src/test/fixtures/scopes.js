const scopes = {
    'admin' : {
        description: {
            en: 'An administrative token',
        },
    },
};

export default scopes;
