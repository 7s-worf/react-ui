import users from './users';
import scopes from './scopes';
import invitations from './invitations';
import signupRequests from './signup_requests';

var fixtures = new Map([
    ['users', users],
    ['scopes', scopes],
    ['invitations', invitations],
    ['signupRequests', signupRequests],
]);

export default fixtures;
