var signupRequests = [
    {
        'created_at': '2018-06-27T19:19:46.112228Z',
        'data': {
            'display_name': null,
            'email': 'max@mustermann.com',
            'invitation': null,
            'language': 'en',
            'password': '$pbkdf2-sha512$150000$SkmptXZuzXnPOUfo/d/7Xw$EV4UEZcL1s9XCRyJLrg9Zl6nP6dlt38wDm3uvHfv2hY6aNyzEo3eBG2f23vd3MFVtO8gfuHqAfTME48.5zcoJw',
            'trust_computer': null,
        },
        'email': 'max@mustermann.com',
        'id': '84be9595-274d-4a40-a4ab-d0ba8c7db077',
        'updated_at': '2018-06-27T19:19:46.112228Z',
    },
];

export default signupRequests;
