import BaseApi from './base';

export default class InvitationApi extends BaseApi {

    constructor(settings){
        super(...arguments);
        this.invitations = settings.get(['apiFixtures','invitations']) || [];
    }

    getInvitations(){
        return new Promise((resolve) => {
            return setTimeout(() => {
                resolve({invitations : this.invitations});
            }, 700);
        });
    }

    createInvitation(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var error = {
                    status: 400,
                    message: 'unauthenticated',
                    errors: {},
                };
                return reject(error);
            }, 700);
        });
    }

    deleteInvitation(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var error = {
                    status: 400,
                    message: 'unauthenticated',
                    errors: {},
                };
                return reject(error);
            }, 700);
        });
    }
}
