import BaseApi from './base';

export default class UserApi extends BaseApi {

    constructor(settings){
        super(...arguments);
        this.users = settings.get(['apiFixtures','users']) || [];
    }

    getUser(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var users = this.users.filter((user) => user.access_tokens.some((accessToken) => accessToken.token === this.accessToken));
                var wrongPwOrUser = {
                    status: 403,
                    message: 'wrong e-mail or password',
                    errors: {},
                };
                if (users.length == 0)
                    return reject(wrongPwOrUser);
                let user = users[0];
                resolve({user : user, access_token : user.access_tokens[0], organization: user.organization});
            }, 700);
        });
    }

    logout(){
        return new Promise((resolve) => {
            return setTimeout(() => {
                this.accessToken = null;
                resolve({message: 'success', status: 200});
            }, 500);
        });
    }

    signup(provider, data){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var users = this.users.filter((user) => user.email === data.email);
                if (users.length > 0)
                    return reject({
                        status: 400,
                        message: this.settings.t(['invalid-data']),
                        errors: {
                            email: this.settings.t(['user-api','email-already-taken']),
                        },
                    });
                resolve({
                    message: this.settings.t(['user-api', 'signup', 'pending-approval']),
                    status: 202,
                });
            }, 300);
        });
    }

    login(provider, data){
        const promise = new Promise((resolve, reject) => {
            return setTimeout(() => {
                var users = this.users.filter((user) => user.email === data.email);
                var wrongPwOrUser = {
                    status: 403,
                    message: 'wrong e-mail or password',
                    errors: {},
                };
                if (users.length == 0)
                    return reject(wrongPwOrUser);
                let user = users[0];
                if (user.password !== data.password)
                    return reject(wrongPwOrUser);
                if (user.otp !== undefined){
                    if (data.otp === undefined)
                        return reject({
                            status: 403,
                            message: 'An OTP code is required.',
                            errors: {
                                otp: 'An OTP token is required',
                            },
                        });
                    if (data.otp !== user.otp)
                        return reject({
                            status: 403,
                            message: 'Invalid OTP code.',
                            errors: {
                                otp: 'Invalid OTP code.',
                            },
                        });
                }
                resolve({
                    user : user,
                    access_token : user.access_tokens[0],
                    organization : user.organization,
                });
            }, 300);});
        promise.then((data) => {this.accessToken = data['access_token'].token;});
        return promise;
    }
}
