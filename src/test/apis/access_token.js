import BaseApi from './base';

export default class AccessTokenApi extends BaseApi {

    constructor(settings){
        super(...arguments);
        this.scopes = settings.get(['apiFixtures','scopes']) || [];
        this.users = settings.get(['apiFixtures','users']) || [];

    }

    getScopes(){
        return new Promise((resolve) => {
            return setTimeout(() => {
                resolve({scopes : this.scopes});
            }, 700);
        });
    }

    getTokens(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var users = this.users.filter((user) => user.access_tokens.some((accessToken) => accessToken.token === this.accessToken));
                var wrongPwOrUser = {
                    status: 403,
                    message: 'unauthenticated',
                    errors: {},
                };
                if (users.length == 0)
                    return reject(wrongPwOrUser);
                const user = users[0];
                resolve({access_tokens : user.access_tokens});
            }, 700);
        });
    }

    createToken(data){
        data.scopes = Array.from(data.scopes).join(',');
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var error = {
                    status: 400,
                    message: 'unauthenticated',
                    errors: {},
                };
                return reject(error);
            }, 700);
        });
    }

    deleteToken(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var error = {
                    status: 400,
                    message: 'unauthenticated',
                    errors: {},
                };
                return reject(error);
            }, 700);
        });
    }
}
