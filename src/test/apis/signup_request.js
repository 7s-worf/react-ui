import BaseApi from './base';

export default class SignupRequestApi extends BaseApi {

    constructor(settings){
        super(...arguments);
        this.signupRequests = settings.get(['apiFixtures','signupRequests']) || [];
    }

    getSignupRequests(){
        return new Promise((resolve) => {
            return setTimeout(() => {
                resolve({signupRequests : this.signupRequests});
            }, 700);
        });
    }

    approveSignupRequest(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var error = {
                    status: 400,
                    message: 'unauthenticated',
                    errors: {},
                };
                return reject(error);
            }, 700);
        });
    }

    deleteSignupRequest(){
        return new Promise((resolve, reject) => {
            return setTimeout(() => {
                var error = {
                    status: 400,
                    message: 'unauthenticated',
                    errors: {},
                };
                return reject(error);
            }, 700);
        });
    }
}
