import UserApi from './user';
import AccessTokenApi from './access_token';
import InvitationApi from './invitation';
import SignupRequestApi from './signup_request';

export default new Map([
    ['user', UserApi],
    ['invitation', InvitationApi],
    ['accessToken', AccessTokenApi],
    ['signupRequest', SignupRequestApi],
]);
