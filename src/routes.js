import Account from 'worf/components/account';
import Newsletter from 'worf/components/newsletter';
import AccessTokens from 'worf/components/account/access_tokens';
import Invitations from 'worf/components/account/invitations';
import SignupRequests from 'worf/components/account/signup_requests';

import {
    SetupOrganization,
    CreateOrganization,
    JoinOrganizationInfo,
} from 'worf/components/account/organization';


var routes = new Map([
    [
        'logout',
        {
            name: 'logout',
            url: '/logout',
            handler: () => ({ component: Account, noUserRequired: true, props: { page: 'logout' } }),
        },
    ],
    [
        'setupOrganization',
        {
            url: '/organization/setup',
            handler: () => ({ component: SetupOrganization, noOrganizationRequired: true, props: {} }),
        },
    ],
    [
        'createOrganization',
        {
            name: 'create-organization',
            url: '/organization/create',
            handler: () => ({ component: CreateOrganization, noOrganizationRequired: true, props: {} }),
        },
    ],
    [
        'joinOrganizationInfo',
        {
            url: '/organization/join-info',
            handler: () => ({ component: JoinOrganizationInfo, noOrganizationRequired: true, props: {} }),
        },
    ],
    [
        'login',
        {
            url: '/login(?:/(.+))?',
            handler: (provider) => ({ component: Account, noUserRequired: true, props: { page: 'login', provider: provider, defaultProvider: 'password' } }),
        },
    ],
    [
        'blockEmail',
        {
            url: '/block-email',
            handler: () => ({ component: Account, noUserRequired: true, props: { page: 'block-email' } }),
        },
    ],
    [
        'confirmSignup',
        {
            url: '/confirm-signup',
            handler: () => ({ component: Account, noUserRequired: true, props: { page: 'confirm-signup' } }),
        },
    ],
    [
        'newsletterConfirm',
        {
            url: '/newsletter/confirm',
            handler: () => ({ component: Newsletter, noUserRequired: true, props: { page: 'confirm' } }),
        },
    ],
    [
        'forgotPassword',
        {
            url: '/forgot-password',
            handler: () => ({ component: Account, noUserRequired: true, props: { page: 'forgot-password' } }),
        },
    ],
    [
        'resetPassword',
        {
            url: '/reset-password',
            handler: () => ({ component: Account, noUserRequired: true, props: { page: 'reset-password' } }),
        },
    ],
    [
        'accessTokens',
        {
            url: '/access-tokens',
            handler: () => ({ title: 'access-tokens', component: AccessTokens }),
        },
    ],
    [
        'newAccessToken',
        {
            url: '/access-tokens/new',
            handler: () => ({ title: 'access-tokens', component: AccessTokens, props: { newToken: true } }),
        },
    ],
    [
        'invitations',
        {
            url: '/invitations',
            handler: () => ({ title: 'invitations', component: Invitations }),
        },
    ],
    [
        'signupRequests',
        {
            url: '/signup-requests',
            handler: () => ({ title: 'signup-requests', component: SignupRequests }),
        },
    ],
    [
        'newInvitation',
        {
            url: '/invitations/new',
            handler: () => ({ title: 'invitations', component: Invitations, props: { newInvitation: true } }),
        },
    ],
    [
        'signup',
        {
            url: '/signup(?:/(.+))?',
            handler: (provider) => ({ component: Account, noUserRequired: true, props: { page: 'signup', provider: provider, defaultProvider: 'password' } }),
        },
    ],
]);

export default routes;
