import Base from '7s/actions/base';

export default class Google extends Base {

    static get defaultKey() {
        return 'google';
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.set({ status: 'initialized' });
    }

    logout(){
        const {status} = this.get();
        if (status == 'logging-out')
            return;

        this.update({status: 'logging-out'});

        const promise = this.googleAuth.signOut();
        promise.then(() => {
            this.set({status: 'logged-out'});
        });
        promise.catch(() => {
            this.set({status: 'logged-out', withErrors: true});
        });
    }

    initialize() {
        const initializeGoogleLogin = () => {
            const googleApi = window.gapi;
            const clientId = this.settings.get(['google', 'clientId']);
            googleApi.load('auth2', () => {
                googleApi.auth2.init({ client_id: clientId }).then((obj) => {
                    this.googleAuth = obj;
                    if (this.googleAuth.isSignedIn.get()){
                        this.persistentSet({});
                        const user = this.googleAuth.currentUser.get();
                        const profile = user.getBasicProfile();
                        const authResponse = user.getAuthResponse(true);
                        this.set({
                            status: 'logged-in',
                            idToken: authResponse.id_token,
                            profile: profile,
                        }
                        );
                    }
                    else{
                        const {loginAttempted} = this.persistentGet();
                        this.set({ status: 'logged-out', loginAttempted: loginAttempted });
                    }
                }).catch(() => this.set({status: 'error'}));
            }, () => this.set({status: 'error'}));
        };
        if (window.gapi === undefined) {
            var script = document.createElement('script');
            script.onload = () => {
                initializeGoogleLogin();
            };
            script.onerror = () => {
                this.set({status: 'error'});
            };
            setTimeout(() => {
                if (this.get().status == 'initialized')
                    this.set({status: 'error'});
            }, 5000);
            script.src = 'https://apis.google.com/js/platform.js';
            document.head.appendChild(script);
        } else {
            initializeGoogleLogin();
        }
    }

    reset(){
        // we sign the user out from Google
        if (this.googleAuth.isSignedIn.get()){
            this.googleAuth.signOut();
        }

    }

    login() {
        this.persistentSet({loginAttempted: true});
        this.set({status: 'logging-in'});
        this.googleAuth.signIn({
            ux_mode: 'redirect',
        });
    }

}
