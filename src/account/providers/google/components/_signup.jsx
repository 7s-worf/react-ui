import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import { T } from '7s/components';
import { ErrorMessage, ErrorFor } from '7s/components/error_helpers';
import Message from '7s/components/message';

const Button = (props) => <div className="has-text-centered google-signup">
    <a disabled={props.disabled || false}
        className="button is-primary is-outlined is-large"
        onClick={props.onClick}>
        <span className="icon is-small">
            <i className="fab fa-google" />
        </span>
        <span>
            {props.children}
        </span>
    </a>
</div>;

const SignupHTMLForm = (props) => {
    const { email, ready, onSubmit, error, genericFields, settings, signingUp } = props;
    return <div>
        <ErrorMessage key="error-message" error={error} settings={settings} />
        <form onSubmit={onSubmit} id="signup" key="signup-form" >
            <fieldset disabled={signingUp}>
                <div className="field" key="email">
                    <label className="label"><T k='email' /></label>
                    <ErrorFor error={error} field='email' settings={settings} />
                    <div className="control has-icons-left">
                        <span className="icon is-small is-left">
                            <i className="fab fa-google" />
                        </span>
                        <input
                            readOnly={true}
                            className="input is-success"
                            value={email}
                            name="email" />
                    </div>
                </div>
                {genericFields}
                <div className="field" key="signup">
                    <div className="control">
                        <button disabled={!ready || signingUp}
                            onClick={(e) => { e.preventDefault(); onSubmit(); }}
                            className="button is-primary">Complete Sign Up</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>;
};

class GoogleSignup extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { googleActions, signupActions } = props;
        googleActions.initialize();
        signupActions.reset();
    }

    componentDidMount() {
        this.checkStatus();
    }

    componentDidUpdate() {
        this.checkStatus();
    }

    checkStatus() {
        const { google, signup, googleActions } = this.props;
        if (signup.status == 'failed' && google.status == 'logged-in'){
            googleActions.reset();
        }
    }

    render() {
        const { props } = this;
        const {
            google,
            googleActions,
            signup,
            signupActions,
            ready,
            genericData,
            genericFields,
            settings } = props;
        const { status, loginAttempted } = google;

        const doSignup = () => {
            signupActions.signup('google', {id_token: google.idToken}, genericData);
        };

        let content;
        switch (status) {
            case 'failed':
                content = <ErrorMessage error={google.error} />;
                break;
            case 'logged-in':
                content = <div>
                    <SignupHTMLForm
                        ready={ready}
                        error={signup.error}
                        genericFields={genericFields}
                        signingUp={signup.status == 'signing-up'}
                        onSubmit={doSignup}
                        email={google.profile.getEmail()}
                        settings={settings} />
                </div>;
                break;
            case 'logging-in':
                content = <Button disabled={true}>
                    Please wait...
                </Button>;
                break;
            case 'logged-out':
                content = <Button onClick={() => googleActions.login()}>
                    Sign Up With Google
                </Button>;
                break;
            case 'error':
                content = <Message type="danger">
                    An error occurred when loading the Google auth module.
                    Please check if your ad-blocker might block loading
                    the script, or try to reload this page.
                </Message>;
                break;
            default:
                content = <Message type="info">
                    Please wait, loading the Google authentication libraries...
                </Message>;
                break;
        }
        return <div>
            {content}
        </div>;
    }
}

export default withSettings(withRouter(withActions(GoogleSignup, ['google', 'signup'])));
