import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import { Message } from '7s/components';
import { ErrorMessage } from '7s/components/error_helpers';


const Button = (props) => <div className="has-text-centered google-login">
    <a disabled={props.disabled || false}
        className="button is-primary is-outlined is-large"
        onClick={props.onClick}>
        <span className="icon is-small">
            <i className="fab fa-google" />
        </span>
        <span>
            {props.children}
        </span>
    </a>
</div>;

class GoogleLogin extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { googleActions, loginActions } = props;
        googleActions.initialize();
        loginActions.reset();
        this.checkStatus();
    }

    componentDidMount() {
        this.checkStatus();
    }

    componentDidUpdate() {
        this.checkStatus();
    }

    checkStatus() {
        const { google, login, googleActions, loginActions, ready, genericData} = this.props;

        if (google.status == 'logged-in' && ready && login.status == 'initialized')
            loginActions.login('google', {id_token: google.idToken}, genericData);
        else if (login.status == 'failed' && google.status == 'logged-in'){
            googleActions.reset();
        }
    }

    render() {
        const { props } = this;
        const { google, googleActions, login, ready, genericFields } = props;
        const { status, loginAttempted } = google;

        let content;

        switch (status) {
            case 'failed':
                content = <ErrorMessage error={google.error} />;
                break;
            case 'logged-in':
                switch(login.status){
                    case 'failed':
                        content = <ErrorMessage error={login.error} />;
                        break;
                    case 'logging-in':
                        content = <Message type="info">
                            Please wait, logging you in...
                        </Message>;
                        break;
                    case 'logged-in':
                        content = <Message type="success">
                            You&apos;ve been successfully logged in. We will redirect
                            you in a second...
                        </Message>;
                        break;
                }
                break;
            case 'logging-in':
                content = <Button disabled={true} onClick={() => googleActions.login()}>
                    Please wait...
                </Button>;
                break;
            case 'logged-out':
                content = <Button disabled={!ready} onClick={() => googleActions.login()}>
                    Login With Google
                </Button>;
                break;
            case 'error':
                content = <Message type="danger">
                    An error occurred when loading the Google auth module.
                    Please check if your ad-blocker might block loading
                    the script, or try to reload this page.
                </Message>;
                break;
            default:
                content = <Message type="info">
                    Please wait, loading the Google authentication libraries...
                </Message>;
                break;
        }
        return <div>
            {content}
            <form>
                {genericFields}
            </form>
        </div>;
    }
}

export default withSettings(withRouter(withActions(GoogleLogin, ['google', 'login'])));
