import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import { ErrorMessage } from '7s/components/error_helpers';

import Message from '7s/components/message';

class GoogleLogout extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { googleActions } = props;
        googleActions.initialize();
        this.checkStatus();
    }

    checkStatus() {
        const { google, googleActions} = this.props;
        const { status } = google;
        if (status == 'logged-in')
            googleActions.logout();
    }

    componentDidUpdate(){
        this.checkStatus();
    }

    render() {
        const { props } = this;
        const { google } = props;
        const { status } = google;

        let content;
        switch (status) {
            case 'failed':
                content = <ErrorMessage error={google.error} />;
                break;
            case 'logged-out':
                content = <Message type="success">
                    You&apos;ve been successfully logged out.
                </Message>;
                break;
            case 'logging-out':
                content = <Message type="info">
                    Please wait, logging you out...
                </Message>;
                break;
            case 'error':
                content = <Message type="danger">
                    An error occurred when loading the Google auth module.
                    Please check if your ad-blocker might block loading
                    the script, or try to reload this page.
                </Message>;
                break;
            default:
                content = <Message type="info">
                    Please wait, loading the Google authentication libraries...
                </Message>;
                break;
        }
        return <div>
            {content}
        </div>;
    }
}

export default withSettings(withRouter(withActions(GoogleLogout, ['google', 'user'])));
