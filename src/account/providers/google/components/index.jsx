import SignupForm from './_signup';
import LoginForm from './_login';
import LogoutForm from './_logout';

export default {
    signup: SignupForm,
    login: LoginForm,
    logout: LogoutForm,
};
