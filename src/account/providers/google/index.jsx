import React from 'react';
import Actions from './actions';
import Components from './components';

const providers = new Map([
    ['google', {
        forms: Components,
        title: 'providers.google',
        icon: <i className="fab fa-google" />,
    }]]);

export function setup(settings) {
    settings.updateWithMap(new Map([
        ['actions', Actions],
        ['accountProviders', providers],
    ]));
}
