import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';
import { Message, Button } from '7s/components';

class GithubSignup extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { signupActions } = props;
        signupActions.reset();
        this.checkCode();
    }

    componentDidUpdate(){
        this.checkCode();
    }

    checkCode(){
        const {router, githubActions, github, signup } = this.props;
        if (signup.status == 'failed' && github.status == 'confirmed')
            githubActions.reset();
        if (router.currentParams.get('code') !== undefined){
            githubActions.setCode(router.currentParams.get('code'), router.currentParams.get('state'));
            if (router.currentParams.get('code') !== undefined){
                // we remove the code from the URL
                router.navigateToUrl(router.currentPath);
            }
        }
    }

    render() {

        const {
            signup,
            signupActions,
            ready,
            genericData,
            github,
            githubActions,
            genericFields,
        } = this.props;

        const redirectToGithub = () => {
            const url = githubActions.redirectUrl('/signup/github');
            window.location.href = url;
        };
        const doSignup = () => {
            if (!ready)
                return;
            signupActions.signup('github', {code: github.code, state: github.state}, genericData);
        };

        if (signup.status == 'signing-up') {
            return <div>
                <Message type="success">
                    Please wait, we&apos;re signing you up...
                </Message>
            </div>;
        } else if (signup.status == 'failed') {
            return <div>
                <Message type="danger">
                    Sorry, something did not work: <strong>{signup.error.message}</strong>
                </Message>
            </div>;
        }

        if (github.status == 'confirmed'){
            return <div>
                <Button primary large outlined onClick={doSignup} disabled={!ready}>
                    Confirm Github Signup
                </Button>
                {genericFields}
            </div>;

        } else {
            return <div>
                <Button primary large outlined icon="github" onClick={redirectToGithub}>
                    Sign Up With Github
                </Button>
            </div>;
        }

    }
}

export default withSettings(withRouter(withActions(GithubSignup, ['github', 'signup'])));
