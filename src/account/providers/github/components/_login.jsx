import React from 'react';
import { Button, Message } from '7s/components';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { withRouter } from '7s/components/router';

class GithubLogin extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { loginActions } = props;
        loginActions.reset();
        this.checkCode();
    }

    componentDidUpdate() {
        this.checkCode();
    }

    checkCode() {
        const { router, route, github, githubActions, genericData, login, loginActions } = this.props;
        if (github.status == 'confirmed') {
            if (login.status == 'initialized')
                loginActions.login('github', {code: github.code, state: github.state}, genericData);
            else
                githubActions.reset();
        } else if (router.currentParams.get('code') !== undefined) {
            githubActions.setCode(router.currentParams.get('code'), router.currentParams.get('state'));
            if (router.currentParams.get('code') !== undefined) {
                // we remove the code from the URL
                router.navigateToUrl(router.currentPath);
            }
        }
    }

    render() {

        const {
            login,
            ready,
            router,
            genericData,
            githubActions,
            genericFields } = this.props;

        const redirectToGithub = () => {
            if (!ready)
                return;
            const url = githubActions.redirectUrl('/login/github', genericData);
            window.location.href = url;
        };

        if (login.status == 'logging-in') {
            return <Message type="success">
                    Please wait, logging you in...
            </Message>;
        } else if (login.status == 'failed') {
            return <Message type="danger">
                    Something went wrong: {login.error.message}.
            </Message>;
        }

        return <div>
            <Button disabled={!ready} primary large outlined icon="github" onClick={() => redirectToGithub()}>
                Log In With Github
            </Button>
            {genericFields}
        </div>;
    }
}

export default withSettings(withRouter(withActions(GithubLogin, ['github', 'login'])));
