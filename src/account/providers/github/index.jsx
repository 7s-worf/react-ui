import React from 'react';
import Actions from './actions';
import Components from './components';

const providers = new Map([
    ['github', {
        forms: Components,
        title: 'providers.github',
        icon: <i className="fab fa-github" />,
    }]]);

export function setup(settings) {
    settings.updateWithMap(new Map([
        ['actions', Actions],
        ['accountProviders', providers],
    ]));
}
