import Github from './github';

export default new Map([
    ['github', Github],
]);
