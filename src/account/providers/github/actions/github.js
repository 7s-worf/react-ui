import Base from '7s/actions/base';
import {encodeQueryData} from '7s/utils/url';

export default class Github extends Base {

    constructor(store, settings, key){
        super(store, settings, key);
        this.reset();
    }

    reset(){
        this.set({status: 'initialized'});
    }

    static get defaultKey(){
        return 'github';
    }
    
    setCode(code, state){
        if (this.get().status == 'confirmed')
            return;
        this.set({status: 'confirmed', code: code, state: state});
    }

    redirectUrl(baseUrl){
        const githubSettings = this.settings.get('github');
        const queryData = encodeQueryData({
            'client_id' : githubSettings.get('clientID'),
            'redirect_uri' : this.settings.get('url')+baseUrl,
            'scope' : githubSettings.get('scope'),
            'state' : 'foo',
        });
        return 'https://github.com/login/oauth/authorize?'+queryData;
    }
}
