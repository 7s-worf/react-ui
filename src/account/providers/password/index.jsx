import React from 'react';
import Components from './components';

const providers = new Map([
    ['password', {
        forms: Components,
        title: 'providers.password',
        icon: <i className="fas fa-key" />,
    }]]);

export function setup(settings) {
    settings.updateWithMap(new Map([
        ['accountProviders', providers],
    ]));
}
