import Form from '7s/utils/form';

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
const passwordRegex = /^.{8,40}$/i;

export default class PasswordSignupForm extends Form {

    validate(){
        const errors = {};
        const {settings, data} = this;
        if (!data.email){
            errors.email = settings.t('account.missing-email');
        }
        else if (!emailRegex.test(data.email)){
            errors.email = settings.t('account.invalid-email');
        }
        if (!passwordRegex.test(data.password || '')){
            errors.password =settings.t('account.invalid-password');
        }
        return errors;
    }
}
