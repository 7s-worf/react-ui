import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { T } from '7s/components';
import withForm from '7s/components/with_form';
import SignupForm from '../forms/signup';

import { ErrorMessage, ErrorFor } from '7s/components/error_helpers';

const SignupHTMLForm = (props) => {
    const { data, error, genericFields, setValue, settings, valid, onSubmit, signingUp } = props;

    return <div>
        <ErrorMessage key="error-message" error={error} settings={settings} />
        <form onSubmit={onSubmit} id="signup" key="signup-form" >
            <fieldset disabled={signingUp}>
                <div className="field" key="email">
                    <label className="label"><T k='account.email' /></label>
                    <ErrorFor error={error} field='email' settings={settings} />
                    <div className="control">
                        <input
                            type=""
                            id=""
                            className="input"
                            onChange={(e) =>  setValue('email', e.target.value) }
                            value={data.email || ''}
                            name="" />
                    </div>
                </div>
                <div className="field" key="password">
                    <label className="label"><T k='account.password' /></label>
                    <ErrorFor error={error} field='password' settings={settings} />
                    <div className="control">
                        <input
                            type="password"
                            id="password"
                            className="input"
                            onChange={(e) => setValue('password', e.target.value) }
                            value={data.password || ''}
                            name="" />
                    </div>
                </div>
                {genericFields}
                <div className="field" key="signup">
                    <div className="control">
                        <button disabled={!valid || signingUp}
                            onClick={(e) => { e.preventDefault(); onSubmit(); }}
                            className="button is-primary">Sign up</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>;
};

class PasswordSignup extends React.Component {

    constructor(props, context) {
        super(props, context);
        const {signupActions} = props;
        signupActions.reset();
    }

    render() {
        const { props } = this;
        const { settings, ready, genericFields, genericData, signupForm, signupActions, signup } = props;
        const { error, data, valid, set } = signupForm;

        const doSignup = () => signupActions.signup('password', data, genericData);

        let formError = error;

        if (signup.status == 'failed')
            formError = signup.error;

        return <SignupHTMLForm
            data={data}
            error={formError}
            valid={valid && ready}
            genericFields={genericFields}
            signingUp={signup.status == 'signing-up'}
            onSubmit={doSignup}
            setValue={set}
            settings={settings} />;
    }
}

export default withForm(
    withSettings(withActions(PasswordSignup, ['signup'])),
    SignupForm,
    'signupForm'
);
