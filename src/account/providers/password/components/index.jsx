import SignupForm from './_signup';
import LoginForm from './_login';

export default {
    signup: SignupForm,
    login: LoginForm,
};
