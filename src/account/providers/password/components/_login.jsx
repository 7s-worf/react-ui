import React from 'react';
import { withSettings } from '7s/components/settings';
import { withActions } from '7s/components/store';
import { T } from '7s/components';
import withForm from '7s/components/with_form';
import LoginForm from '../forms/login';

import { ErrorMessage, ErrorFor } from '7s/components/error_helpers';

const LoginHTMLForm = (props) => {
    const { data, error, genericFields, set, settings, valid, onSubmit, loggingIn } = props;

    return <div>
        <ErrorMessage key="error-message" error={error} settings={settings} />
        <form onSubmit={onSubmit} id="login" key="login-form" >
            <fieldset disabled={loggingIn}>
                <div className="field" key="email">
                    <label className="label"><T k='account.email' /></label>
                    <ErrorFor error={error} field='email' settings={settings} />
                    <div className="control">
                        <input
                            type=""
                            id=""
                            className="input"
                            onChange={(e) => set('email', e.target.value) }
                            value={data.email || ''}
                            name="" />
                    </div>
                </div>
                <div className="field" key="password">
                    <label className="label"><T k='account.password' /></label>
                    <ErrorFor error={error} field='password' settings={settings} />
                    <div className="control">
                        <input
                            type="password"
                            id="password"
                            className="input"
                            onChange={(e) => set('password', e.target.value) }
                            value={data.password || ''}
                            name="" />
                    </div>
                </div>
                {genericFields}
                <div className="field" key="login">
                    <div className="control">
                        <button disabled={!valid || loggingIn}
                            onClick={(e) => { e.preventDefault(); onSubmit(); }}
                            className="button is-primary">Log in</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>;
};

class PasswordLogin extends React.Component {

    constructor(props, context) {
        super(props, context);
        const {loginActions} = props;
        loginActions.reset();
    }

    render() {
        const { props } = this;
        const { settings,
            genericFields,
            genericData,
            loginForm,
            loginActions,
            login } = props;

        const { error, data, valid, set } = loginForm;
    
        const doLogin = () => {
            loginActions.login('password', data, genericData);
        };

        let formError = error;

        if (login.status == 'failed')
            formError = login.error;

        return <LoginHTMLForm
            data={data}
            error={formError}
            valid={valid}
            genericFields={genericFields}
            loggingIn={login.status == 'logging-in'}
            onSubmit={doLogin}
            set={set}
            settings={settings} />;
    }
}

export default withForm(
    withSettings(withActions(PasswordLogin, ['login'])), LoginForm, 'loginForm');
