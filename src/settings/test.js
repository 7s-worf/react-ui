import Settings from '7s/utils/settings';

import fixtures from 'worf/test/fixtures';
import apis from 'worf/test/apis';
import baseSettings from './_base';

const settings = new Settings([]);

settings.update(baseSettings);
settings.update(new Settings([
    ['env', 'test'],
    ['apiFixtures', fixtures],
    ['apis', apis],
]));

export default settings;
