import Settings from '7s/utils/settings';
import baseSettings from './_base';

const settings = new Settings();
settings.update(baseSettings);
settings.update(new Settings(new Map([
    ['env', 'development'],
    ['url', 'http://localhost:8080'],
    ['userApiUrl', 'http://localhost:5000'],
    ['github', new Map([
        ['clientID', '5ef22b56caf7837b3da6'],
    ])],
    ['google', 
        new Map([['clientId', '1094985475488-52kb3fan7hop95qps65e5hjuchl3kuri.apps.googleusercontent.com']]),
    ],
])));

export default settings;
