import Settings from '7s/utils/settings';
import baseSettings from './_base';

const settings = new Settings();
settings.update(baseSettings);

export default settings;
