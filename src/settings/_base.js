import Settings from '7s/utils/settings';
import {convertYAMLTranslations} from '7s/utils/i18n';
import yamlTranslations from './translations';
import Actions from 'worf/actions';
import APIs from 'worf/apis';
import routes from 'worf/routes';
import menu from 'worf/components/menu';

// we load different providers
import {setup as githubSetup} from 'worf/account/providers/github';
import {setup as passwordSetup} from 'worf/account/providers/password';
import {setup as googleSetup} from 'worf/account/providers/google';

const settings = new Settings([
    ['translations', convertYAMLTranslations(yamlTranslations)],
    ['showTitles', true],
    ['routes', routes],
    ['menu', menu],
    ['actions', Actions],
    ['apis', APIs],
    ['google', 
        new Map([['clientId', 'please-provide-me']]),
    ],
    [
        'github',
        new Map([
            ['clientID', 'please-provide-me'],
            ['scope', 'read:user,user:email'],
        ]),
    ],
]);

githubSetup(settings);
googleSetup(settings);
passwordSetup(settings);

export default settings;
