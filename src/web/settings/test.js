import Settings from '7s/utils/settings';

import genericSettings from 'worf/settings/test';
import baseSettings from './_base';

const settings = new Settings();
settings.update(genericSettings);
settings.update(baseSettings);

export default settings;
