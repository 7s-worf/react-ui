import BaseApi from './base';

export default class SignupRequestApi extends BaseApi {

    get baseUrl(){
        return this.settings.get(['userApiUrl']);
    }

    getSignupRequests(){
        return this.authenticatedRequest({
            url: 'signup-requests',
            method: 'GET',
        });
    }

    approveSignupRequest(signupRequestId){
        return this.authenticatedRequest({
            url: 'signup-requests/'+signupRequestId,
            method: 'POST',
        });
    }

    deleteSignupRequest(signupRequestId){
        return this.authenticatedRequest({
            url: 'signup-requests/'+signupRequestId,
            method: 'DELETE',
        });
    }
}
