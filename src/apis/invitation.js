import BaseApi from './base';

export default class InvitationApi extends BaseApi {

    get baseUrl(){
        return this.settings.get(['userApiUrl']);
    }

    getInvitations(){
        return this.authenticatedRequest({
            url: 'invitations',
            method: 'GET',
        });
    }

    createInvitation(data){
        return this.authenticatedRequest({
            url: 'invitations',
            json: data,
            method: 'POST',
        });
    }

    deleteInvitation(invitationId){
        return this.authenticatedRequest({
            url: 'invitations/'+invitationId,
            method: 'DELETE',
        });
    }
}
