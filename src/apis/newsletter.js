import BaseApi from './base';

export default class NewsletterApi extends BaseApi {

    get baseUrl() {
        return this.settings.get(['userApiUrl']);
    }

    confirmNewsletter(data) {
        const promise = this.request({
            url: 'newsletter/confirm',
            params: data,
            method: 'GET',
        });
        return promise;
    }

}
