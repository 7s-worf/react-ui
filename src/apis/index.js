import VersionApi from './version';
import UserApi from './user';
import AccessTokenApi from './access_token';
import InvitationApi from './invitation';
import SignupRequestApi from './signup_request';
import BlockEMailApi from './block_email';
import NewsletterApi from './newsletter';
import OrganizationApi from './organization';

export default new Map([
    ['user', UserApi],
    ['newsletter', NewsletterApi],
    ['organization', OrganizationApi],
    ['version', VersionApi],
    ['blockEMail', BlockEMailApi],
    ['accessToken', AccessTokenApi],
    ['invitation', InvitationApi],
    ['signupRequest', SignupRequestApi],
]);
