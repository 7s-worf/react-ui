import BaseApi from './base';

export default class UserApi extends BaseApi {

    get baseUrl(){
        return '';
    }

    get version(){
        return '';
    }

    getVersion(){
        return this.request({
            url: 'version.json',
            method: 'GET',
        });
    }

}
