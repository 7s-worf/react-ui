import BaseApi from './base';

export default class BlockEMailApi extends BaseApi {

    get baseUrl(){
        return this.settings.get(['userApiUrl']);
    }

    blockEMail(code){
        return this.authenticatedRequest({
            url: 'block-email',
            params: {
                code: code,
            },
            method: 'GET',
        });
    }

}
