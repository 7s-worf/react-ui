import BaseApi from './base';

export default class AccessTokenApi extends BaseApi {

    get baseUrl(){
        return this.settings.get(['userApiUrl']);
    }

    getScopes(){
        return this.authenticatedRequest({
            url: 'access-token-scopes',
            method: 'GET',
        });
    }

    getTokens(){
        return this.authenticatedRequest({
            url: 'access-tokens',
            method: 'GET',
        });
    }

    createToken(data){
        data.scopes = Array.from(data.scopes);
        return this.authenticatedRequest({
            url: 'access-tokens',
            json: data,
            method: 'POST',
        });
    }

    deleteToken(tokenId){
        return this.authenticatedRequest({
            url: 'access-tokens/'+tokenId,
            method: 'DELETE',
        });
    }
}
