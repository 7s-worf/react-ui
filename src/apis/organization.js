import BaseApi from './base';

export default class OrganizationApi extends BaseApi {

    get baseUrl() {
        return this.settings.get(['userApiUrl']);
    }

    getOrganization() {
        return this.authenticatedRequest({
            url: 'organization',
            method: 'GET',
        });
    }

    create(data) {
        return this.authenticatedRequest({
            url: 'organizations',
            json: data,
            method: 'POST',
        });
    }

    update(organizationId, data) {
        return this.authenticatedRequest({
            url: 'organizations/'+organizationId,
            json: data,
            method: 'PUT',
        });
    }

    delete(organizationId) {
        return this.authenticatedRequest({
            url: 'organizations/'+organizationId,
            method: 'DELETE',
        });
    }

}
