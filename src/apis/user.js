import BaseApi from './base';

export default class UserApi extends BaseApi {

    get baseUrl() {
        return this.settings.get(['userApiUrl']);
    }

    getUser() {
        return this.authenticatedRequest({
            url: 'user',
            method: 'GET',
        });
    }

    signup(provider, data) {
        const promise = this.request({
            url: 'signup/'+provider,
            json: data,
            method: 'POST',
        });
        promise.then((data) => {
            if (data.status == 201)
                this.accessToken = data['access_token'].token;
        });
        return promise;
    }

    confirmSignup(data) {
        const promise = this.request({
            url: 'confirm-signup',
            params: data,
            method: 'GET',
        });
        promise.then((data) => {
            this.accessToken = data['access_token'].token;
        });
        return promise;
    }

    logout() {
        const promise = this.authenticatedRequest({
            url: 'logout',
            method: 'POST',
        });
        promise.then((data) => {
            this.accessToken = null;
        });
        return promise;
    }

    login(provider, data) {
        const promise = this.request({
            url: 'login/' + provider,
            json: data,
            method: 'POST',
        });
        promise.then((data) => {
            if (data.status == 201)
                this.accessToken = data['access_token'].token;
        });
        return promise;
    }

}
