import Base from '7s/actions/base';

import InvitationForm from 'worf/forms/account/invitation';

class Invitation extends Base {

    constructor(store, settings, key){
        super(store, settings, key);
        var InvitationApi = settings.get(['apis','invitation']);
        this.api = new InvitationApi(settings, store);
    }

}

export class CreateInvitation extends Invitation {

    static get defaultKey() {
        return 'createInvitation';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.reset();
    }

    setData(key, value){
        const {formData} = this.get();
        formData[key] = value;
        const form = new InvitationForm(formData, this.settings);
        this.update({formData: formData, valid: form.valid, error: form.error});
    }

    reset(){
        this.set({status: 'initialized', formData: {}});
    }

    createInvitation(){
        const {formData, status} = this.get();
        if (status == 'creating')
            return;
        this.update({status: 'creating'});
        return this.handle(
            this.api.createInvitation(formData),
            (data) => {
                this.set({status: 'success', data: data.invitation});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }


}

export class DeleteInvitation extends Invitation {

    static get defaultKey() {
        return 'deleteInvitation';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

    deleteInvitation(invitationId){
        this.update({status: 'deleting'});
        return this.handle(
            this.api.deleteInvitation(invitationId),
            (data) => {
                this.set({status: 'success'});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }

}

export class Invitations extends Invitation {

    static get defaultKey() {
        return 'invitations';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

    getInvitations(){
        const {status, updating} = this.get();
        if (status == 'loaded'){
            if (updating)
                return;
            this.update({status: 'loaded', updating: true});
        }
        else
            this.set({status: 'loading'});
        return this.handle(
            this.api.getInvitations(),
            (data) => {
                this.set({data : data.invitations, status: 'loaded'});
            },
            (error) => {
                this.set({status: 'failed', error: error});
            }
        );
    }

}
