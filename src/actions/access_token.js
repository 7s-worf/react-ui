import Base from '7s/actions/base';
import AccessTokenForm from 'worf/forms/account/access_token';

class AccessToken extends Base {

    constructor(store, settings, key){
        super(store, settings, key);
        var AccessTokenApi = settings.get(['apis','accessToken']);
        this.api = new AccessTokenApi(settings, store);
    }

}

export class CreateAccessToken extends AccessToken {

    static get defaultKey(){
        return 'createAccessToken';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.reset();
    }

    reset(){
        this.set({status: 'initialized', formData: {}});
    }

    setData(key, value){
        const data = this.get().formData;
        data[key] = value;
        const form = new AccessTokenForm(data, this.settings);
        this.update({formData: data, valid: form.valid, error: form.error});
    }

    createToken(){
        const data = this.get().formData;
        this.update({status: 'creating'});
        return this.handle(
            this.api.createToken(data),
            (data) => {
                this.set({status: 'success', token: data.access_token});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }
}

export class DeleteAccessToken extends AccessToken {

    static get defaultKey(){
        return 'deleteAccessToken';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

    deleteToken(tokenId){
        this.update({status: 'deleting'});
        return this.handle(
            this.api.deleteToken(tokenId),
            (data) => {
                this.set({status: 'success'});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }

}

export class AccessTokenScopes extends AccessToken {

    static get defaultKey(){
        return 'accessTokenScopes';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

    getScopes(){
        const {status, updating} = this.get();
        if (status == 'loaded'){
            if (updating)
                return;
            this.update({status: 'loaded', updating: true});
        }
        else
            this.set({status: 'loading'});
        return this.handle(
            this.api.getScopes(),
            (data) => {
                this.update({data : data.scopes, status: 'loaded'});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }

}

export class AccessTokens extends AccessToken {

    static get defaultKey(){
        return 'accessTokens';
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

    getTokens(){
        const {status, updating} = this.get();
        if (status == 'loaded'){
            if (updating)
                return;
            this.update({status: 'loaded', updating: true});
        }
        else
            this.set({status: 'loading'});
        return this.handle(
            this.api.getTokens(),
            (data) => {
                this.set({data : data.access_tokens, status: 'loaded'});
            },
            (error) => {
                this.set({status: 'failed', error: error});
            }
        );
    }

}

