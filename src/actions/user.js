import Base from '7s/actions/base';

export default class User extends Base {

    static get defaultKey() {
        return 'user';
    }

    getUser(){
        if (this.get().status == 'verifying-login')
            return;
        this.set({status: 'verifying-login'});
        if (this.api.accessToken == null)
            return this.set({status: 'logged-out'});
        return this.handle(this.api.getUser(),
            data=>{
                const {access_token: accessToken, user, organization} = data;
                this.setUserData(user, accessToken, organization);
            },
            error=>{
                this.set({status: 'logged-out', error: error});
                this.api.accessToken=null;
            });
    }

    setUserData(user, accessToken, organization){
        this.set({
            user: user,
            organization: organization,
            accessToken: accessToken,
            status: 'logged-in',
            provider: accessToken.data !== null ? accessToken.data.provider : undefined,
        });
    }

    setLoggedOut(){
        this.set({
            status: 'logged-out',
            provider: this.get().provider,
        });
    }
    
    constructor(store, settings, key) {
        super(store, settings, key);

        var UserApi = settings.get(['apis', 'user']);
        this.api = new UserApi(settings, store);
        this.set({});
        this.getUser();
    }

}
