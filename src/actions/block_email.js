import Base from '7s/actions/base';

export default class BlockEMail extends Base {

    static get defaultKey() {
        return 'blockEMail';
    }

    blockEMail(code){
        if (this.get().status === 'blocking')
            return;
        this.set({status: 'blocking'});
        this.handle(this.api.blockEMail(code), (data) => {
            this.set({status: 'succeeded', data: data});
        }, (data) => {
            this.set({status: 'failed', data: data});
        });
    }

    constructor(store, settings, key){
        super(store, settings, key);
        var BlockEMailApi = settings.get(['apis','blockEMail']);
        this.api = new BlockEMailApi(settings, store);
        this.set({
            status: 'initialized',
        });
    }

}
