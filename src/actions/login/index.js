import Base from '7s/actions/base';

export default class Login extends Base {

    static get defaultKey() {
        return 'login';
    }

    login(provider, loginData, genericData) {
        const {status} = this.get();

        if (status == 'logging-in' || status == 'logged-in')
            return;

        this.set({status: 'logging-in'});

        const data = {};

        for(let d of [loginData, genericData || {}]){
            for(let [key, value] of Object.entries(d)){
                data[key] = value;
            }
        }

        return this.handle(this.api.login(provider, data),
            (data) => {
                this.loginProvider = provider;
                this.set({
                    status: 'logged-in',
                    user: data.user,
                    organization: data.organization,
                    accessToken: data.access_token,
                });
            },
            (data) => {
                this.set({ status: 'failed', error: data });
            }
        );
    }

    reset(){
        this.set({status: 'initialized'});
    }

    get loginProvider(){
        return this.persistentGet().loginProvider;
    }

    set loginProvider(provider){
        // we store the login provider persistently so that we can take the
        // appropriate action when logging the user out.
        this.persistentUpdate({loginProvider : provider});
    }

    constructor(store, settings, key) {
        super(store, settings, key);

        var UserApi = settings.get(['apis', 'user']);
        this.api = new UserApi(settings, store);

        this.reset();
    }

}
