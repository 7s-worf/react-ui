import Base from '7s/actions/base';

class SignupRequest extends Base {

    constructor(store, settings, key){
        super(store, settings, key);
        var SignupRequestApi = settings.get(['apis','signupRequest']);
        this.api = new SignupRequestApi(settings, store);
    }

}

export class SignupRequests extends SignupRequest {

    static get defaultKey() {
        return 'signupRequests';
    }

    getSignupRequests(){
        const {status, updating} = this.get();
        if (status == 'loaded'){
            if (updating)
                return;
            this.update({status: 'loaded', updating: true});
        }
        else
            this.set({status: 'loading'});
        return this.handle(
            this.api.getSignupRequests(),
            (data) => {
                this.set({data : data.requests, status: 'loaded'});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

}

export class ApproveSignupRequest extends SignupRequest {

    static get defaultKey() {
        return 'approveSignupRequest';
    }

    approveSignupRequest(signupRequestId){
        this.update({status: 'approving'});
        return this.handle(
            this.api.approveSignupRequest(signupRequestId),
            (data) => {
                this.set({status: 'success'});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

}

export class DeleteSignupRequest extends SignupRequest {

    static get defaultKey() {
        return 'deleteSignupRequest';
    }

    deleteSignupRequest(signupRequestId){
        this.update({status: 'deleting'});
        return this.handle(
            this.api.deleteSignupRequest(signupRequestId),
            (data) => {
                this.set({status: 'success'});
            },
            (error) => {
                this.update({status: 'failed', error: error});
            }
        );
    }

    constructor(store, settings, key){
        super(store, settings, key);
        this.set({status: 'initialized'});
    }

}
