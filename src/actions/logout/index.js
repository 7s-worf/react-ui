import Base from '7s/actions/base';

export default class Logout extends Base {

    static get defaultKey() {
        return 'logout';
    }

    logout() {

        const {status} = this.get();

        if (status === 'logged-out' ||
            status === 'logging-out')
            return;

        this.update({ status: 'logging-out' });

        return this.handle(this.api.logout(),
            data => {
                this.set({ status: 'logged-out', data: data });
            },
            error => {
                this.set({ status: 'logged-out', error: error });
            });
    }

    reset(){
        this.set({status: 'initialized'});
    }


    constructor(store, settings, key) {
        super(store, settings, key);
        var UserApi = settings.get(['apis', 'user']);
        this.api = new UserApi(settings, store);
        this.reset();
    }

}
