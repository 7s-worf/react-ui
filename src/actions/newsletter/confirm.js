import Base from '7s/actions/base';

export default class ConfirmSubscription extends Base {

    static get defaultKey() {
        return 'confirmSubscription';
    }

    confirm(data){
        const {status} = this.get();
        if (status == 'confirming')
            return;
        this.set({status: 'confirming'});
        return this.handle(this.api.confirmNewsletter(data),
            data=>{
                this.set({
                    status: data.status == 201 ? 'succeeded' : 'already-confirmed',
                });
            },
            error=>{this.set({status: 'failed', error: error});},
        );
    }

    reset(){
        this.set({status: 'initialized'});
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        var NewsletterApi = settings.get(['apis', 'newsletter']);
        this.api = new NewsletterApi(settings, store);
        this.reset();
    }

}
