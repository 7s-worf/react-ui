import User from './user';

import {
    CreateOrganization,
} from './organization';

import {
    AccessTokenScopes,
    AccessTokens,
    CreateAccessToken,
    DeleteAccessToken,
} from './access_token';

import {
    Invitations,
    CreateInvitation,
    DeleteInvitation,
} from './invitation';

import {
    ConfirmSubscription,
} from './newsletter';

import {
    SignupRequests,
    ApproveSignupRequest,
    DeleteSignupRequest,
} from './signup_request';

import Version from './version';
import BlockEMail from './block_email';

import Login from './login';

import ConfirmSignup from './signup/confirm';
import Signup from './signup';

import Logout from './logout';

export default new Map([
    ['user', User],
    ['version', Version],
    ['blockEMail', BlockEMail],

    // organization related actions
    ['createOrganization', CreateOrganization],

    // access token related actions
    ['accessTokenScopes', AccessTokenScopes],
    ['accessTokens', AccessTokens],
    ['createAccessToken', CreateAccessToken],
    ['deleteAccessToken', DeleteAccessToken],

    // signup request related actions
    ['signupRequests', SignupRequests],
    ['approveSignupRequest', ApproveSignupRequest],
    ['deleteSignupRequest', DeleteSignupRequest],

    // invitation related actions
    ['invitations', Invitations],
    ['createInvitation', CreateInvitation],
    ['deleteInvitation', DeleteInvitation],

    // login related actions
    ['login', Login],

    // signup related actions
    ['confirmSignup', ConfirmSignup],
    ['signup', Signup],
    ['logout', Logout],

    // newsletter-related actions
    ['confirmSubscription', ConfirmSubscription],

]);
