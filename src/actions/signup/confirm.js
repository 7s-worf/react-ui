import Base from '7s/actions/base';

export default class ConfirmSignup extends Base {

    static get defaultKey() {
        return 'confirmSignup';
    }

    confirmSignup(data){
        const {status} = this.get();
        if (status == 'confirming')
            return;
        this.set({status: 'confirming'});
        return this.handle(this.api.confirmSignup(data),
            data=>{
                this.set({
                    status: 'succeeded',
                    user: data.user,
                    accessToken: data.access_token,
                });
            },
            error=>{this.set({status: 'failed', error: error});},
        );
    }

    reset(){
        this.set({status: 'initialized'});
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        var UserApi = settings.get(['apis', 'user']);
        this.api = new UserApi(settings, store);
        this.reset();
    }

}
