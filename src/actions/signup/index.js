import Base from '7s/actions/base';

export default class Signup extends Base {

    static get defaultKey() {
        return 'signup';
    }

    signup(provider, signupData, genericData) {
        const {status} = this.get();

        if (status == 'signing-up' || status == 'logged-in')
            return;

        this.set({status: 'signing-up'});

        const data = {
            language: this.settings.lang(),
        };

        for(let d of [signupData, genericData || {}]){
            for(let [key, value] of Object.entries(d)){
                data[key] = value;
            }
        }

        return this.handle(this.api.signup(provider, data),
            (data) => {
                switch(data.status){
                    case 200: // waiting to confirm
                        this.set({status: 'pending-confirmation'});
                        break;
                    case 201: // signup complete
                        this.set({
                            status: 'logged-in',
                            user: data.user,
                            organization: data.organization,
                            accessToken: data.access_token});
                        break;
                    case 202: // waiting for approval
                        this.set({status: 'pending-approval'});
                        break;
                    default:
                        this.set({status: 'failed', error: {}});
                }
            },
            (data) => {
                this.set({ status: 'failed', error: data });
            }
        );
    }

    reset(){
        this.set({status: 'initialized'});
    }

    constructor(store, settings, key) {
        super(store, settings, key);

        var UserApi = settings.get(['apis', 'user']);
        this.api = new UserApi(settings, store);

        this.reset();
    }

}
