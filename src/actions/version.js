import Base from '7s/actions/base';

export default class Version extends Base {

    static get defaultKey() {
        return 'version';
    }

    getVersion(){
        const {status} = this.get();
        if (status === 'updating' || status === 'loading')
            return;
        if (status === 'loaded')
            this.set({status: 'updating'});
        else
            this.set({status: 'loading'});
        return this.handle(this.api.getVersion(),
            data=>{
                this.set({
                    data: data,
                    status: 'loaded',
                });
            },
            error=>{
                this.set({status: 'failed', error: error});
            });
    }

    constructor(store, settings, key){
        super(store, settings, key);
        var VersionApi = settings.get(['apis','version']);
        this.api = new VersionApi(settings, store);
        if (this.get() === undefined)
            this.set({status: 'undefined'});
        if (this.get().status === 'undefined'){
            this.getVersion();
        }
    }

}
