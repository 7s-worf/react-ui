import FormActions from '7s/actions/form';
import Form from '7s/utils/form';

export class CreateOrganizationForm extends Form {

    validate(){
        const errors = {};
        const {settings, data} = this;
        if (!data.name){
            errors.name = settings.t(['organization', 'missing-name']);
        }
        else if (!/^[\w\d-\s]{2,40}$/i.exec(data.name)){
            errors.name = settings.t(['organization', 'invalid-name']);
        }
        return errors;
    }
}

export default class OrganizationForm extends FormActions {

    static get defaultKey() {
        return 'organizationForm';
    }

    get Form(){
        return CreateOrganizationForm;
    }

}
