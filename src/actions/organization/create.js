import Base from '7s/actions/base';

export default class CreateOrganization extends Base {

    static get defaultKey() {
        return 'createOrganization';
    }

    reset(){
        this.set({status: 'initialized'});
    }

    create(data){
        const {status} = this.get();

        if (status == 'creating')
            return;

        this.set({status: 'creating', data: data});

        return this.handle(
            this.api.create(data),
            (data) => {
                this.set({
                    status: 'succeeded',
                    data: data,
                });
            },
            (error) => {
                console.log(error);
                this.set({
                    status: 'failed',
                    error: error,
                });
            }
        );
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        var OrganizationApi = settings.get(['apis', 'organization']);
        this.api = new OrganizationApi(settings, store);
        this.reset();
    }

}
