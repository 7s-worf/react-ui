import Settings from '7s/utils/settings';

import baseSettings from './_base';
import genericSettings from 'worf/settings/test';

var settings = new Settings();

settings.update(baseSettings);
settings.update(genericSettings);

export default settings;
