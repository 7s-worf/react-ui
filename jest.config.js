// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html
/* eslint-env node */
const path = require('path');

module.exports = {
    // Automatically clear mock calls and instances between every test
    clearMocks: true,
    // The directory where Jest should output its coverage files
    coverageDirectory: 'build/coverage',
    moduleNameMapper: {
        '^7s/(.*)$': '<rootDir>../../7s-ui/src/$1',
    },
    setupFilesAfterEnv: ['<rootDir>src/setupTests.js'],
    // The glob patterns Jest uses to detect test files
    testMatch: [
        '**/__tests__/**/*.[jt]s?(x)',
        '**/?(*.)+(spec).[tj]s?(x)',
    ],
};
