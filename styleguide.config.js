// This is the configuration file for react-styleguidist
/* eslint-env node */
module.exports = {
    sections: [
        {
            name: 'Account',
            components: 'src/components/account/*.{jsx,tsx}',
            sections: [
                {
                    name: 'Access Tokens',
                    components: 'src/components/account/access_tokens/**/*.{jsx,tsx}',
                },
                {
                    name: 'Forms',
                    components: 'src/components/account/forms/**/*.{jsx,tsx}',
                },
                {
                    name: 'Invitations',
                    components: 'src/components/account/invitations/*.{jsx,tsx}',
                },
                {
                    name: 'Organization',
                    components: 'src/components/account/organization/**/*.{jsx,tsx}',
                },
                {
                    name: 'Signup Requests',
                    components: 'src/components/account/signup_requests/*.{jsx,tsx}',
                },
            ],
        },
        {
            name: 'Newsletter',
            components: 'src/components/newsletter/**/*.{jsx,tsx}',
        },
        {
            name: 'Others',
            components: 'src/components/*.{jsx,tsx}',
        },
    ],
    styleguideDir: 'public',
    title: 'Worf React Components',
};
